<?php
/* --------------------------------------------------------------
WP CUSTOMIZE SECTION - CUSTOM SETTINGS
-------------------------------------------------------------- */

add_action( 'customize_register', 'yanbal_customize_register' );

function yanbal_customize_register( $wp_customize ) {

    /* SOCIAL SETTINGS */
    $wp_customize->add_section('ybl_social_settings', array(
        'title'    => __('Redes Sociales', 'yanbal'),
        'description' => __('Agregue aqui las redes sociales de la página, serán usadas globalmente', 'yanbal'),
        'priority' => 175,
    ));

    $wp_customize->add_setting('ybl_social_settings[facebook]', array(
        'default'           => '',
        'sanitize_callback' => 'yanbal_sanitize_url',
        'capability'        => 'edit_theme_options',
        'type'           => 'option',
    ));

    $wp_customize->add_control( 'facebook', array(
        'type' => 'url',
        'section' => 'ybl_social_settings',
        'settings' => 'ybl_social_settings[facebook]',
        'label' => __( 'Facebook', 'yanbal' ),
    ));

    $wp_customize->add_setting('ybl_social_settings[twitter]', array(
        'default'           => '',
        'sanitize_callback' => 'yanbal_sanitize_url',
        'capability'        => 'edit_theme_options',
        'type'           => 'option',
    ));

    $wp_customize->add_control( 'twitter', array(
        'type' => 'url',
        'section' => 'ybl_social_settings',
        'settings' => 'ybl_social_settings[twitter]',
        'label' => __( 'Twitter', 'yanbal' ),
    ));

    $wp_customize->add_setting('ybl_social_settings[instagram]', array(
        'default'           => '',
        'sanitize_callback' => 'yanbal_sanitize_url',
        'capability'        => 'edit_theme_options',
        'type'           => 'option',

    ));

    $wp_customize->add_control( 'instagram', array(
        'type' => 'url',
        'section' => 'ybl_social_settings',
        'settings' => 'ybl_social_settings[instagram]',
        'label' => __( 'Instagram', 'yanbal' ),
    ));

    $wp_customize->add_setting('ybl_social_settings[linkedin]', array(
        'default'           => '',
        'sanitize_callback' => 'yanbal_sanitize_url',
        'capability'        => 'edit_theme_options',
        'type'           => 'option',
    ));

    $wp_customize->add_control( 'linkedin', array(
        'type' => 'url',
        'section' => 'ybl_social_settings',
        'settings' => 'ybl_social_settings[linkedin]',
        'label' => __( 'LinkedIn', 'yanbal' ),
    ));

    $wp_customize->add_setting('ybl_social_settings[youtube]', array(
        'default'           => '',
        'sanitize_callback' => 'yanbal_sanitize_url',
        'capability'        => 'edit_theme_options',
        'type'           => 'option',

    ));

    $wp_customize->add_control( 'youtube', array(
        'type' => 'url',
        'section' => 'ybl_social_settings',
        'settings' => 'ybl_social_settings[youtube]',
        'label' => __( 'YouTube', 'yanbal' ),
    ) );

    /* COOKIES SETTINGS */
    $wp_customize->add_section('ybl_cookie_settings', array(
        'title'    => __('Cookies', 'yanbal'),
        'description' => __('Opciones de Cookies', 'yanbal'),
        'priority' => 176,
    ));

    $wp_customize->add_setting('ybl_cookie_settings[cookie_text]', array(
        'default'           => '',
        'sanitize_callback' => 'sanitize_text_field',
        'capability'        => 'edit_theme_options',
        'type'           => 'option'

    ));

    $wp_customize->add_control( 'cookie_text', array(
        'type' => 'textarea',
        'label'    => __('Cookie consent', 'yanbal'),
        'description' => __( 'Texto del Cookie consent.' ),
        'section'  => 'ybl_cookie_settings',
        'settings' => 'ybl_cookie_settings[cookie_text]'
    ));

    $wp_customize->add_setting('ybl_cookie_settings[cookie_link]', array(
        'default'           => '',
        'sanitize_callback' => 'absint',
        'capability'        => 'edit_theme_options',
        'type'           => 'option',

    ));

    $wp_customize->add_control( 'cookie_link', array(
        'type'     => 'dropdown-pages',
        'section' => 'ybl_cookie_settings',
        'settings' => 'ybl_cookie_settings[cookie_link]',
        'label' => __( 'Link de Cookies', 'yanbal' ),
    ) );

    $wp_customize->add_section('ybl_sendinblue_settings', array(
        'title'    => __('Sendinblue', 'startravel'),
        'description' => __('Opciones para Sendinblue', 'startravel'),
        'priority' => 180
    ));

    $wp_customize->add_setting('ybl_sendinblue_settings[apikey]', array(
        'default'           => '',
        'sanitize_callback' => 'sanitize_text_field',
        'capability'        => 'edit_theme_options',
        'type'           => 'option'
    ));

    $wp_customize->add_control('apikey', array(
        'type' => 'text',
        'label'    => __('APIKey', 'startravel'),
        'description' => __('Agregar APIkey de Sendinblue', 'startravel'),
        'section'  => 'ybl_sendinblue_settings',
        'settings' => 'ybl_sendinblue_settings[apikey]'
    ));

    $wp_customize->add_setting('ybl_sendinblue_settings[b2c_thanks_link]', array(
        'default'           => '',
        'sanitize_callback' => 'yanbal_sanitize_url',
        'capability'        => 'edit_theme_options',
        'type'           => 'option',

    ));

    $wp_customize->add_control( 'b2c_thanks_link', array(
        'type' => 'url',
        'section' => 'ybl_sendinblue_settings',
        'settings' => 'ybl_sendinblue_settings[b2c_thanks_link]',
        'label' => __( 'Link de Destino del Form B2C', 'yanbal' ),
    ) );

    $wp_customize->add_setting('ybl_sendinblue_settings[b2b_thanks_link]', array(
        'default'           => '',
        'sanitize_callback' => 'yanbal_sanitize_url',
        'capability'        => 'edit_theme_options',
        'type'           => 'option',

    ));

    $wp_customize->add_control( 'b2b_thanks_link', array(
        'type' => 'url',
        'section' => 'ybl_sendinblue_settings',
        'settings' => 'ybl_sendinblue_settings[b2b_thanks_link]',
        'label' => __( 'Link de Destino del Form B2B', 'yanbal' ),
    ) );
}

function yanbal_sanitize_url( $url ) {
    return esc_url_raw( $url );
}