<?php
/* --------------------------------------------------------------
    1.- WELCOME HERO
-------------------------------------------------------------- */
$cmb_b2c_hero = new_cmb2_box( array(
    'id'            => $prefix . 'b2c_hero_metabox',
    'title'         => esc_html__( '1.- Hero Principal', 'yanbal' ),
    'object_types'  => array( 'page' ),
    'show_on'      => array( 'key' => 'page-template', 'value' => 'templates/page-landing-b2c.php' ),
    'context'    => 'normal',
    'priority'   => 'high',
    'show_names' => true,
    'cmb_styles' => true,
    'closed'     => false
) );


$cmb_b2c_hero->add_field( array(
    'id'         => $prefix . 'main_bg',
    'name'      => esc_html__( 'Imagen de Fondo del Hero', 'yanbal' ),
    'desc'      => esc_html__( 'Cargue una imagen de fondo para este hero', 'yanbal' ),
    'type'    => 'file',
    'options' => array(
        'url' => false
    ),
    'text'    => array(
        'add_upload_file_text' => esc_html__( 'Cargar Fondo', 'yanbal' ),
    ),
    'query_args' => array(
        'type' => array(
            'image/gif',
            'image/jpeg',
            'image/png'
        )
    ),
    'preview_size' => 'medium'
) );

/* --------------------------------------------------------------
    2.- POPUP INFO
-------------------------------------------------------------- */
$cmb_b2c_popup = new_cmb2_box( array(
    'id'            => $prefix . 'b2c_popup_metabox',
    'title'         => esc_html__( '2.- Popup Principal', 'yanbal' ),
    'object_types'  => array( 'page' ),
    'show_on'      => array( 'key' => 'page-template', 'value' => 'templates/page-landing-b2c.php' ),
    'context'    => 'normal',
    'priority'   => 'high',
    'show_names' => true,
    'cmb_styles' => true,
    'closed'     => false
) );


$cmb_b2c_popup->add_field( array(
    'id'         => $prefix . 'popup_image',
    'name'      => esc_html__( 'Imagen del PopUp', 'yanbal' ),
    'desc'      => esc_html__( 'Cargue una imagen para este PopUp', 'yanbal' ),
    'type'    => 'file',
    'options' => array(
        'url' => false
    ),
    'text'    => array(
        'add_upload_file_text' => esc_html__( 'Cargar Imagen', 'yanbal' ),
    ),
    'query_args' => array(
        'type' => array(
            'image/gif',
            'image/jpeg',
            'image/png'
        )
    ),
    'preview_size' => 'medium'
) );

$cmb_b2c_popup->add_field( array(
    'id'         => $prefix . 'popup_content',
    'name'      => esc_html__( 'Texto del Popup', 'yanbal' ),
    'desc'      => esc_html__( 'Ingrese un Texto descriptivo para el Popup', 'yanbal' ),
    'type' => 'wysiwyg',
    'options' => array(
        'textarea_rows' => get_option('default_post_edit_rows', 2),
        'teeny' => false
    )
) );

$cmb_b2c_popup->add_field( array(
    'id'         => $prefix . 'popup_sendinblue',
    'name'      => esc_html__( 'ID de la lista de Sendinblue a agregar', 'yanbal' ),
    'desc'      => esc_html__( 'Ingrese un ID de la lista de Sendinblue a agregar para el Popup', 'yanbal' ),
    'type' => 'text'
) );