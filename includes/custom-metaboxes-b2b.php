<?php
/* --------------------------------------------------------------
    1.- WELCOME HERO
-------------------------------------------------------------- */
$cmb_b2b_hero = new_cmb2_box( array(
    'id'            => $prefix . 'b2b_hero_metabox',
    'title'         => esc_html__( '1.- Hero Principal', 'yanbal' ),
    'object_types'  => array( 'page' ),
    'show_on'      => array( 'key' => 'page-template', 'value' => 'templates/page-landing-b2b.php' ),
    'context'    => 'normal',
    'priority'   => 'high',
    'show_names' => true,
    'cmb_styles' => true,
    'closed'     => false
) );


$cmb_b2b_hero->add_field( array(
    'id'         => $prefix . 'main_bg',
    'name'      => esc_html__( 'Imagen de Fondo del Hero', 'yanbal' ),
    'desc'      => esc_html__( 'Cargue una imagen de fondo para este hero', 'yanbal' ),
    'type'    => 'file',
    'options' => array(
        'url' => false
    ),
    'text'    => array(
        'add_upload_file_text' => esc_html__( 'Cargar Fondo', 'yanbal' ),
    ),
    'query_args' => array(
        'type' => array(
            'image/gif',
            'image/jpeg',
            'image/png'
        )
    ),
    'preview_size' => 'medium'
) );

$cmb_b2b_hero->add_field( array(
    'id'         => $prefix . 'button_text',
    'name'      => esc_html__( 'Texto del Boton Principal', 'yanbal' ),
    'desc'      => esc_html__( 'Ingrese el texto para el boton del hero', 'yanbal' ),
    'type' => 'text'
) );

$cmb_b2b_hero->add_field( array(
    'id'         => $prefix . 'button_url',
    'name'      => esc_html__( 'Link URL del Boton Principal', 'yanbal' ),
    'desc'      => esc_html__( 'Ingrese el link para el boton del hero', 'yanbal' ),
    'type'      => 'text_url'
) );

/* --------------------------------------------------------------
    2.- QUIENES SOMOS
-------------------------------------------------------------- */
$cmb_b2b_about = new_cmb2_box( array(
    'id'            => $prefix . 'b2b_about_metabox',
    'title'         => esc_html__( '2.- Quienes Somos', 'yanbal' ),
    'object_types'  => array( 'page' ),
    'show_on'      => array( 'key' => 'page-template', 'value' => 'templates/page-landing-b2b.php' ),
    'context'    => 'normal',
    'priority'   => 'high',
    'show_names' => true,
    'cmb_styles' => true,
    'closed'     => false
) );

$cmb_b2b_about->add_field( array(
    'id'         => $prefix . 'about_icon',
    'name'      => esc_html__( 'Ícono del Titulo Principal', 'yanbal' ),
    'desc'      => esc_html__( 'Cargue una imagen de icono para esta sección', 'yanbal' ),
    'type'    => 'file',
    'options' => array(
        'url' => false
    ),
    'text'    => array(
        'add_upload_file_text' => esc_html__( 'Cargar Ícono', 'yanbal' ),
    ),
    'query_args' => array(
        'type' => array(
            'image/gif',
            'image/jpeg',
            'image/png'
        )
    ),
    'preview_size' => 'medium'
) );

$cmb_b2b_about->add_field( array(
    'id'         => $prefix . 'about_title',
    'name'      => esc_html__( 'Título Principal de la sección', 'yanbal' ),
    'desc'      => esc_html__( 'Ingrese el título principal para esta sección', 'yanbal' ),
    'type' => 'text'
) );

$cmb_b2b_about->add_field( array(
    'id'         => $prefix . 'about_intro',
    'name'      => esc_html__( 'Intro Principal de la sección', 'yanbal' ),
    'desc'      => esc_html__( 'Ingrese el intro principal para esta sección', 'yanbal' ),
    'type' => 'wysiwyg',
    'options' => array(
        'textarea_rows' => get_option('default_post_edit_rows', 2),
        'teeny' => false
    )
) );

$group_field_id = $cmb_b2b_about->add_field( array(
    'id'          => $prefix . 'about_box_group',
    'name'       => esc_html__( 'Boxes Flotantes dentro de Sección', 'yanbal' ),
    'description' => __( 'Gropo de Boxes Flotantes en lista ordenable', 'yanbal' ),
    'type'        => 'group',
    'options'     => array(
        'group_title'       => __( 'Box {#}', 'yanbal' ),
        'add_button'        => __( 'Agregar otro Box', 'yanbal' ),
        'remove_button'     => __( 'Remover Box', 'yanbal' ),
        'sortable'          => true,
        'closed'         => true,
        'remove_confirm' => esc_html__( '¿Esta seguro de remover este Box?', 'yanbal' )
    )
) );

$cmb_b2b_about->add_group_field( $group_field_id, array(
    'id'         => 'image',
    'name'      => esc_html__( 'Imagen del Box Flotante', 'yanbal' ),
    'desc'      => esc_html__( 'Cargue una imagen para el Box Flotante de la sección', 'yanbal' ),
    'type'    => 'file',
    'options' => array(
        'url' => false
    ),
    'text'    => array(
        'add_upload_file_text' => esc_html__( 'Cargar Imagen', 'yanbal' ),
    ),
    'query_args' => array(
        'type' => array(
            'image/gif',
            'image/jpeg',
            'image/png'
        )
    ),
    'preview_size' => 'medium'
) );

$cmb_b2b_about->add_group_field( $group_field_id, array(
    'id'         => 'title',
    'name'      => esc_html__( 'Título Principal del box flotante', 'yanbal' ),
    'desc'      => esc_html__( 'Ingrese el título principal para este box', 'yanbal' ),
    'type' => 'text'
) );

$cmb_b2b_about->add_group_field( $group_field_id, array(
    'id'         => 'content',
    'name'      => esc_html__( 'Contenido Principal del box flotante', 'yanbal' ),
    'desc'      => esc_html__( 'Ingrese el Contenido principal para este box', 'yanbal' ),
    'type' => 'wysiwyg',
    'options' => array(
        'textarea_rows' => get_option('default_post_edit_rows', 2),
        'teeny' => false
    )
) );

/* --------------------------------------------------------------
    3.- BENEFICIOS
-------------------------------------------------------------- */
$cmb_b2b_benefits = new_cmb2_box( array(
    'id'            => $prefix . 'b2b_benefits_metabox',
    'title'         => esc_html__( '3.- Beneficios', 'yanbal' ),
    'object_types'  => array( 'page' ),
    'show_on'      => array( 'key' => 'page-template', 'value' => 'templates/page-landing-b2b.php' ),
    'context'    => 'normal',
    'priority'   => 'high',
    'show_names' => true,
    'cmb_styles' => true,
    'closed'     => false
) );


$cmb_b2b_benefits->add_field( array(
    'id'         => $prefix . 'benefits_icon',
    'name'      => esc_html__( 'Ícono del Titulo Principal', 'yanbal' ),
    'desc'      => esc_html__( 'Cargue una imagen de icono para esta sección', 'yanbal' ),
    'type'    => 'file',
    'options' => array(
        'url' => false
    ),
    'text'    => array(
        'add_upload_file_text' => esc_html__( 'Cargar Ícono', 'yanbal' ),
    ),
    'query_args' => array(
        'type' => array(
            'image/gif',
            'image/jpeg',
            'image/png'
        )
    ),
    'preview_size' => 'medium'
) );

$cmb_b2b_benefits->add_field( array(
    'id'         => $prefix . 'benefits_title',
    'name'      => esc_html__( 'Título Principal de la sección', 'yanbal' ),
    'desc'      => esc_html__( 'Ingrese el título principal para esta sección', 'yanbal' ),
    'type' => 'text'
) );

$group_field_id = $cmb_b2b_benefits->add_field( array(
    'id'          => $prefix . 'benefits_group',
    'name'       => esc_html__( 'Beneficios dentro de Sección', 'yanbal' ),
    'description' => __( 'Gropo de Beneficios en lista ordenable', 'yanbal' ),
    'type'        => 'group',
    'options'     => array(
        'group_title'       => __( 'Beneficio {#}', 'yanbal' ),
        'add_button'        => __( 'Agregar otro Beneficio', 'yanbal' ),
        'remove_button'     => __( 'Remover Beneficio', 'yanbal' ),
        'sortable'          => true,
        'closed'         => true,
        'remove_confirm' => esc_html__( '¿Esta seguro de remover este Beneficio?', 'yanbal' )
    )
) );

$cmb_b2b_benefits->add_group_field( $group_field_id, array(
    'id'         => 'image',
    'name'      => esc_html__( 'Imagen del Beneficio', 'yanbal' ),
    'desc'      => esc_html__( 'Cargue una imagen para el Beneficio', 'yanbal' ),
    'type'    => 'file',
    'options' => array(
        'url' => false
    ),
    'text'    => array(
        'add_upload_file_text' => esc_html__( 'Cargar Imagen', 'yanbal' ),
    ),
    'query_args' => array(
        'type' => array(
            'image/gif',
            'image/jpeg',
            'image/png'
        )
    ),
    'preview_size' => 'medium'
) );

$cmb_b2b_benefits->add_group_field( $group_field_id, array(
    'id'         => 'content',
    'name'      => esc_html__( 'Contenido Principal del Beneficio', 'yanbal' ),
    'desc'      => esc_html__( 'Ingrese el Contenido principal para este Beneficio', 'yanbal' ),
    'type' => 'wysiwyg',
    'options' => array(
        'textarea_rows' => get_option('default_post_edit_rows', 2),
        'teeny' => false
    )
) );

/* --------------------------------------------------------------
    4.- INSTRUCCIONES
-------------------------------------------------------------- */
$cmb_b2b_inst = new_cmb2_box( array(
    'id'            => $prefix . 'b2b_inst_metabox',
    'title'         => esc_html__( '4.- Instrucciones', 'yanbal' ),
    'object_types'  => array( 'page' ),
    'show_on'      => array( 'key' => 'page-template', 'value' => 'templates/page-landing-b2b.php' ),
    'context'    => 'normal',
    'priority'   => 'high',
    'show_names' => true,
    'cmb_styles' => true,
    'closed'     => false
) );

$cmb_b2b_inst->add_field( array(
    'id'         => $prefix . 'inst_main_bg',
    'name'      => esc_html__( 'Imagen de Fondo del Hero', 'yanbal' ),
    'desc'      => esc_html__( 'Cargue una imagen de fondo para este hero', 'yanbal' ),
    'type'    => 'file',
    'options' => array(
        'url' => false
    ),
    'text'    => array(
        'add_upload_file_text' => esc_html__( 'Cargar Fondo', 'yanbal' ),
    ),
    'query_args' => array(
        'type' => array(
            'image/gif',
            'image/jpeg',
            'image/png'
        )
    ),
    'preview_size' => 'medium'
) );


$cmb_b2b_inst->add_field( array(
    'id'         => $prefix . 'inst_icon',
    'name'      => esc_html__( 'Ícono del Titulo Principal', 'yanbal' ),
    'desc'      => esc_html__( 'Cargue una imagen de icono para esta sección', 'yanbal' ),
    'type'    => 'file',
    'options' => array(
        'url' => false
    ),
    'text'    => array(
        'add_upload_file_text' => esc_html__( 'Cargar Ícono', 'yanbal' ),
    ),
    'query_args' => array(
        'type' => array(
            'image/gif',
            'image/jpeg',
            'image/png'
        )
    ),
    'preview_size' => 'medium'
) );

$cmb_b2b_inst->add_field( array(
    'id'         => $prefix . 'inst_title',
    'name'      => esc_html__( 'Título Principal de la sección', 'yanbal' ),
    'desc'      => esc_html__( 'Ingrese el título principal para esta sección', 'yanbal' ),
    'type' => 'text'
) );

$group_field_id = $cmb_b2b_inst->add_field( array(
    'id'          => $prefix . 'inst_group',
    'name'       => esc_html__( 'Instrucciones dentro de Sección', 'yanbal' ),
    'description' => __( 'Gropo de Instrucciones en lista ordenable', 'yanbal' ),
    'type'        => 'group',
    'options'     => array(
        'group_title'       => __( 'Paso {#}', 'yanbal' ),
        'add_button'        => __( 'Agregar otro Paso', 'yanbal' ),
        'remove_button'     => __( 'Remover Paso', 'yanbal' ),
        'sortable'          => true,
        'closed'         => true,
        'remove_confirm' => esc_html__( '¿Esta seguro de remover este Paso?', 'yanbal' )
    )
) );

$cmb_b2b_inst->add_group_field( $group_field_id, array(
    'id'         => 'image',
    'name'      => esc_html__( 'Imagen del Paso', 'yanbal' ),
    'desc'      => esc_html__( 'Cargue una imagen para el Paso', 'yanbal' ),
    'type'    => 'file',
    'options' => array(
        'url' => false
    ),
    'text'    => array(
        'add_upload_file_text' => esc_html__( 'Cargar Imagen', 'yanbal' ),
    ),
    'query_args' => array(
        'type' => array(
            'image/gif',
            'image/jpeg',
            'image/png'
        )
    ),
    'preview_size' => 'medium'
) );

$cmb_b2b_inst->add_group_field( $group_field_id, array(
    'id'         => 'title',
    'name'      => esc_html__( 'Título Principal del Paso', 'yanbal' ),
    'desc'      => esc_html__( 'Ingrese el Contenido principal para este Paso', 'yanbal' ),
    'type' => 'text'
) );

$cmb_b2b_inst->add_group_field( $group_field_id, array(
    'id'         => 'content',
    'name'      => esc_html__( 'Contenido Principal del Paso', 'yanbal' ),
    'desc'      => esc_html__( 'Ingrese el Contenido principal para este Paso', 'yanbal' ),
    'type' => 'wysiwyg',
    'options' => array(
        'textarea_rows' => get_option('default_post_edit_rows', 2),
        'teeny' => false
    )
) );

$cmb_b2b_inst->add_field( array(
    'id'         => $prefix . 'inst_button_text',
    'name'      => esc_html__( 'Texto del Boton Principal', 'yanbal' ),
    'desc'      => esc_html__( 'Ingrese el texto para el boton de la sección', 'yanbal' ),
    'type' => 'text'
) );

$cmb_b2b_inst->add_field( array(
    'id'         => $prefix . 'inst_button_url',
    'name'      => esc_html__( 'Link URL del Boton Principal', 'yanbal' ),
    'desc'      => esc_html__( 'Ingrese el link para el boton de la sección', 'yanbal' ),
    'type'      => 'text_url'
) );

/* --------------------------------------------------------------
    5.- TESTIMONIOS
-------------------------------------------------------------- */
$cmb_b2b_test = new_cmb2_box( array(
    'id'            => $prefix . 'b2b_test_metabox',
    'title'         => esc_html__( '5.- Testimonios', 'yanbal' ),
    'object_types'  => array( 'page' ),
    'show_on'      => array( 'key' => 'page-template', 'value' => 'templates/page-landing-b2b.php' ),
    'context'    => 'normal',
    'priority'   => 'high',
    'show_names' => true,
    'cmb_styles' => true,
    'closed'     => false
) );

$cmb_b2b_test->add_field( array(
    'id'         => $prefix . 'test_icon',
    'name'      => esc_html__( 'Ícono del Titulo Principal', 'yanbal' ),
    'desc'      => esc_html__( 'Cargue una imagen de icono para esta sección', 'yanbal' ),
    'type'    => 'file',
    'options' => array(
        'url' => false
    ),
    'text'    => array(
        'add_upload_file_text' => esc_html__( 'Cargar Ícono', 'yanbal' ),
    ),
    'query_args' => array(
        'type' => array(
            'image/gif',
            'image/jpeg',
            'image/png'
        )
    ),
    'preview_size' => 'medium'
) );

$cmb_b2b_test->add_field( array(
    'id'         => $prefix . 'test_title',
    'name'      => esc_html__( 'Título Principal de la sección', 'yanbal' ),
    'desc'      => esc_html__( 'Ingrese el título principal para esta sección', 'yanbal' ),
    'type' => 'text'
) );

$group_field_id = $cmb_b2b_test->add_field( array(
    'id'          => $prefix . 'test_group',
    'name'       => esc_html__( 'Testimonios dentro de Sección', 'yanbal' ),
    'description' => __( 'Gropo de Testimonios en lista ordenable', 'yanbal' ),
    'type'        => 'group',
    'options'     => array(
        'group_title'       => __( 'Testimonio {#}', 'yanbal' ),
        'add_button'        => __( 'Agregar otro Testimonio', 'yanbal' ),
        'remove_button'     => __( 'Remover Testimonio', 'yanbal' ),
        'sortable'          => true,
        'closed'         => true,
        'remove_confirm' => esc_html__( '¿Esta seguro de remover este Testimonio?', 'yanbal' )
    )
) );

$cmb_b2b_test->add_group_field( $group_field_id, array(
    'id'         => 'image',
    'name'      => esc_html__( 'Imagen del Testimonio', 'yanbal' ),
    'desc'      => esc_html__( 'Cargue una imagen para el Testimonio', 'yanbal' ),
    'type'    => 'file',
    'options' => array(
        'url' => false
    ),
    'text'    => array(
        'add_upload_file_text' => esc_html__( 'Cargar Imagen', 'yanbal' ),
    ),
    'query_args' => array(
        'type' => array(
            'image/gif',
            'image/jpeg',
            'image/png'
        )
    ),
    'preview_size' => 'medium'
) );

$cmb_b2b_test->add_group_field( $group_field_id, array(
    'id'         => 'author',
    'name'      => esc_html__( 'Autor Principal del Testimonio', 'yanbal' ),
    'desc'      => esc_html__( 'Ingrese el Autor principal para este Testimonio', 'yanbal' ),
    'type' => 'text'
) );

$cmb_b2b_test->add_group_field( $group_field_id, array(
    'id'         => 'content',
    'name'      => esc_html__( 'Contenido Principal del Testimonio', 'yanbal' ),
    'desc'      => esc_html__( 'Ingrese el Contenido principal para este Testimonio', 'yanbal' ),
    'type' => 'wysiwyg',
    'options' => array(
        'textarea_rows' => get_option('default_post_edit_rows', 2),
        'teeny' => false
    )
) );

/* --------------------------------------------------------------
    6.- CONTACTO
-------------------------------------------------------------- */
$cmb_b2b_contact = new_cmb2_box( array(
    'id'            => $prefix . 'b2b_contact_metabox',
    'title'         => esc_html__( '6.- Contacto', 'yanbal' ),
    'object_types'  => array( 'page' ),
    'show_on'      => array( 'key' => 'page-template', 'value' => 'templates/page-landing-b2b.php' ),
    'context'    => 'normal',
    'priority'   => 'high',
    'show_names' => true,
    'cmb_styles' => true,
    'closed'     => false
) );

$cmb_b2b_contact->add_field( array(
    'id'         => $prefix . 'contact_icon',
    'name'      => esc_html__( 'Ícono del Titulo Principal', 'yanbal' ),
    'desc'      => esc_html__( 'Cargue una imagen de icono para esta sección', 'yanbal' ),
    'type'    => 'file',
    'options' => array(
        'url' => false
    ),
    'text'    => array(
        'add_upload_file_text' => esc_html__( 'Cargar Ícono', 'yanbal' ),
    ),
    'query_args' => array(
        'type' => array(
            'image/gif',
            'image/jpeg',
            'image/png'
        )
    ),
    'preview_size' => 'medium'
) );

$cmb_b2b_contact->add_field( array(
    'id'         => $prefix . 'contact_title',
    'name'      => esc_html__( 'Título Principal de la sección', 'yanbal' ),
    'desc'      => esc_html__( 'Ingrese el título principal para esta sección', 'yanbal' ),
    'type' => 'text'
) );

$cmb_b2b_contact->add_field( array(
    'id'         => $prefix . 'contact_intro',
    'name'      => esc_html__( 'Intro Principal de la sección', 'yanbal' ),
    'desc'      => esc_html__( 'Ingrese el intro principal para esta sección', 'yanbal' ),
    'type' => 'wysiwyg',
    'options' => array(
        'textarea_rows' => get_option('default_post_edit_rows', 2),
        'teeny' => false
    )
) );

$cmb_b2b_contact->add_field( array(
    'id'         => $prefix . 'contact_sendinblue',
    'name'      => esc_html__( 'ID de la lista de Sendinblue a agregar', 'yanbal' ),
    'desc'      => esc_html__( 'Ingrese un ID de la lista de Sendinblue a agregar para el Popup', 'yanbal' ),
    'type' => 'text'
) );