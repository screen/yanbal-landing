<?php
/*
function yanbal_custom_post_type() {

	$labels = array(
		'name'                  => _x( 'Clientes', 'Post Type General Name', 'yanbal' ),
		'singular_name'         => _x( 'Cliente', 'Post Type Singular Name', 'yanbal' ),
		'menu_name'             => __( 'Clientes', 'yanbal' ),
		'name_admin_bar'        => __( 'Clientes', 'yanbal' ),
		'archives'              => __( 'Archivo de Clientes', 'yanbal' ),
		'attributes'            => __( 'Atributos de Cliente', 'yanbal' ),
		'parent_item_colon'     => __( 'Cliente Padre:', 'yanbal' ),
		'all_items'             => __( 'Todos los Clientes', 'yanbal' ),
		'add_new_item'          => __( 'Agregar Nuevo Cliente', 'yanbal' ),
		'add_new'               => __( 'Agregar Nuevo', 'yanbal' ),
		'new_item'              => __( 'Nuevo Cliente', 'yanbal' ),
		'edit_item'             => __( 'Editar Cliente', 'yanbal' ),
		'update_item'           => __( 'Actualizar Cliente', 'yanbal' ),
		'view_item'             => __( 'Ver Cliente', 'yanbal' ),
		'view_items'            => __( 'Ver Clientes', 'yanbal' ),
		'search_items'          => __( 'Buscar Cliente', 'yanbal' ),
		'not_found'             => __( 'No hay resultados', 'yanbal' ),
		'not_found_in_trash'    => __( 'No hay resultados en Papelera', 'yanbal' ),
		'featured_image'        => __( 'Imagen del Cliente', 'yanbal' ),
		'set_featured_image'    => __( 'Colocar Imagen del Cliente', 'yanbal' ),
		'remove_featured_image' => __( 'Remover Imagen del Cliente', 'yanbal' ),
		'use_featured_image'    => __( 'Usar como Imagen del Cliente', 'yanbal' ),
		'insert_into_item'      => __( 'Insertar en Cliente', 'yanbal' ),
		'uploaded_to_this_item' => __( 'Cargado a este Cliente', 'yanbal' ),
		'items_list'            => __( 'Listado de Clientes', 'yanbal' ),
		'items_list_navigation' => __( 'Navegación del Listado de Cliente', 'yanbal' ),
		'filter_items_list'     => __( 'Filtro del Listado de Clientes', 'yanbal' ),
	);
	$args = array(
		'label'                 => __( 'Cliente', 'yanbal' ),
		'description'           => __( 'Portafolio de Clientes', 'yanbal' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail' ),
		'taxonomies'            => array( 'tipos-de-clientes' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 15,
		'menu_icon'             => 'dashicons-businessperson',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'post',
		'show_in_rest'          => true,
	);
	register_post_type( 'clientes', $args );

}

add_action( 'init', 'yanbal_custom_post_type', 0 );
*/
