<?php
function ed_metabox_include_front_page( $display, $meta_box ) {
    if ( ! isset( $meta_box['show_on']['key'] ) ) {
        return $display;
    }

    if ( 'front-page' !== $meta_box['show_on']['key'] ) {
        return $display;
    }

    $post_id = 0;

    // If we're showing it based on ID, get the current ID
    if ( isset( $_GET['post'] ) ) {
        $post_id = $_GET['post'];
    } elseif ( isset( $_POST['post_ID'] ) ) {
        $post_id = $_POST['post_ID'];
    }

    if ( ! $post_id ) {
        return false;
    }

    // Get ID of page set as front page, 0 if there isn't one
    $front_page = get_option( 'page_on_front' );

    // there is a front page set and we're on it!
    return $post_id == $front_page;
}
add_filter( 'cmb2_show_on', 'ed_metabox_include_front_page', 10, 2 );

function be_metabox_show_on_slug( $display, $meta_box ) {
    if ( ! isset( $meta_box['show_on']['key'], $meta_box['show_on']['value'] ) ) {
        return $display;
    }

    if ( 'slug' !== $meta_box['show_on']['key'] ) {
        return $display;
    }

    $post_id = 0;

    // If we're showing it based on ID, get the current ID
    if ( isset( $_GET['post'] ) ) {
        $post_id = $_GET['post'];
    } elseif ( isset( $_POST['post_ID'] ) ) {
        $post_id = $_POST['post_ID'];
    }

    if ( ! $post_id ) {
        return $display;
    }

    $slug = get_post( $post_id )->post_name;

    // See if there's a match
    return in_array( $slug, (array) $meta_box['show_on']['value']);
}
add_filter( 'cmb2_show_on', 'be_metabox_show_on_slug', 10, 2 );

add_action( 'cmb2_admin_init', 'yanbal_register_custom_metabox' );
function yanbal_register_custom_metabox() {
    $prefix = 'ybl_';

    require_once('custom-metaboxes-welcome.php');
    require_once('custom-metaboxes-b2c.php');
    require_once('custom-metaboxes-b2b.php');

    
/* --------------------------------------------------------------
    1.- WELCOME HERO
-------------------------------------------------------------- */
$cmb_thanks_welcome = new_cmb2_box( array(
    'id'            => $prefix . 'thanks_hero_metabox',
    'title'         => esc_html__( '1.- Hero Principal', 'yanbal' ),
    'object_types'  => array( 'page' ),
    'show_on'      => array( 'key' => 'page-template', 'value' => 'templates/templates-thanks.php' ),
    'context'    => 'normal',
    'priority'   => 'high',
    'show_names' => true,
    'cmb_styles' => true,
    'closed'     => false
) );


$cmb_thanks_welcome->add_field( array(
    'id'         => $prefix . 'welcome_bg',
    'name'      => esc_html__( 'Imagen de Fondo del Hero', 'yanbal' ),
    'desc'      => esc_html__( 'Cargue una imagen de fondo para este hero', 'yanbal' ),
    'type'    => 'file',
    'options' => array(
        'url' => false
    ),
    'text'    => array(
        'add_upload_file_text' => esc_html__( 'Cargar Fondo', 'yanbal' ),
    ),
    'query_args' => array(
        'type' => array(
            'image/gif',
            'image/jpeg',
            'image/png'
        )
    ),
    'preview_size' => 'medium'
) );

$cmb_thanks_welcome->add_field( array(
    'id'         => $prefix . 'button_text',
    'name'      => esc_html__( 'Texto del Boton Principal', 'yanbal' ),
    'desc'      => esc_html__( 'Ingrese el texto para el boton del hero', 'yanbal' ),
    'type' => 'text'
) );

$cmb_thanks_welcome->add_field( array(
    'id'         => $prefix . 'button_url',
    'name'      => esc_html__( 'Link URL del Boton Principal', 'yanbal' ),
    'desc'      => esc_html__( 'Ingrese el link para el boton del hero', 'yanbal' ),
    'type'      => 'text_url'
) );

}

