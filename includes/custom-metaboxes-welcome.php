<?php
/* --------------------------------------------------------------
    1.- WELCOME HERO
-------------------------------------------------------------- */
$cmb_welcome = new_cmb2_box( array(
    'id'            => $prefix . 'welcome_hero_metabox',
    'title'         => esc_html__( '1.- Hero Principal', 'yanbal' ),
    'object_types'  => array( 'page' ),
    'show_on'      => array( 'key' => 'page-template', 'value' => 'templates/page-splash-welcome.php' ),
    'context'    => 'normal',
    'priority'   => 'high',
    'show_names' => true,
    'cmb_styles' => true,
    'closed'     => false
) );


$cmb_welcome->add_field( array(
    'id'         => $prefix . 'welcome_bg',
    'name'      => esc_html__( 'Imagen de Fondo del Hero', 'yanbal' ),
    'desc'      => esc_html__( 'Cargue una imagen de fondo para este hero', 'yanbal' ),
    'type'    => 'file',
    'options' => array(
        'url' => false
    ),
    'text'    => array(
        'add_upload_file_text' => esc_html__( 'Cargar Fondo', 'yanbal' ),
    ),
    'query_args' => array(
        'type' => array(
            'image/gif',
            'image/jpeg',
            'image/png'
        )
    ),
    'preview_size' => 'medium'
) );

$group_field_id = $cmb_welcome->add_field( array(
    'id'          => $prefix . 'welcome_sites_group',
    'name'       => esc_html__( 'Listado de Sitios en Hero', 'yanbal' ),
    'description' => __( 'Gropo de sitios en lista ordenable', 'yanbal' ),
    'type'        => 'group',
    'options'     => array(
        'group_title'       => __( 'Sitio {#}', 'yanbal' ),
        'add_button'        => __( 'Agregar otro Sitio', 'yanbal' ),
        'remove_button'     => __( 'Remover Sitio', 'yanbal' ),
        'sortable'          => true,
        'closed'         => true,
        'remove_confirm' => esc_html__( '¿Esta seguro de remover este sitio?', 'yanbal' )
    )
) );

$cmb_welcome->add_group_field( $group_field_id, array(
    'id'         => 'bg_image',
    'name'      => esc_html__( 'Fondo del Box', 'yanbal' ),
    'desc'      => esc_html__( 'Cargue un fondo azul para este Box', 'yanbal' ),
    'type'    => 'file',
    'options' => array(
        'url' => false
    ),
    'text'    => array(
        'add_upload_file_text' => esc_html__( 'Cargar Fondo', 'yanbal' ),
    ),
    'query_args' => array(
        'type' => array(
            'image/gif',
            'image/jpeg',
            'image/png'
        )
    ),
    'preview_size' => 'medium'
) );


$cmb_welcome->add_group_field( $group_field_id, array(
    'id'        => 'title',
    'name'      => esc_html__( 'Nombre de la Persona', 'yanbal' ),
    'desc'      => esc_html__( 'Ingrese el nombre para este item', 'yanbal' ),
    'type' => 'text'
) );

$cmb_welcome->add_group_field( $group_field_id, array(
    'id'        => 'url',
    'name'      => esc_html__( 'Cargo del Item', 'yanbal' ),
    'desc'      => esc_html__( 'Ingrese el cargo para este item', 'yanbal' ),
    'type'      => 'text_url'
) );