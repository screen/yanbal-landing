<form id="formFull" class="sendinblue-fullform-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
    <input id="listID" type="hidden" name="listID" value="<?php echo get_post_meta(get_the_ID(), 'ybl_contact_sendinblue', true); ?>">
    <div class="container">
        <div class="row">
            <div class="form-input-item col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                <input id="inputName" type="text" name="form-name" class="form-control form-name-input" placeholder="<?php _e("Nombre *", "yanbal"); ?>" aria-label="<?php _e("Escribe tu Nombre.", "yanbal"); ?>" />
                <small id="errorName" class="invalid-feedback error-name d-none">
                    <?php _e('El nombre no es válido', 'yanbal'); ?>
                </small>
            </div>
            <div class="form-input-item col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                <input id="inputLast"  type="text" name="form-lastname" class="form-control form-lastname-input" placeholder="<?php _e("Apellido *", "yanbal"); ?>" aria-label="<?php _e("Escribe tu Apellido.", "yanbal"); ?>" />
                <small id="errorLast" class="invalid-feedback error-last d-none">
                    <?php _e('El apellido no es válido', 'yanbal'); ?>
                </small>
            </div>
            
            <div class="form-input-item col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                <input id="inputEmail" type="email" class="form-control form-email-input" placeholder="<?php _e("Correo Electrónico *", "yanbal"); ?>" aria-label="<?php _e("Escribe tu Correo Electrónico.", "yanbal"); ?>" />
                <small id="errorEmail" class="invalid-feedback  error-email d-none">
                    <?php _e('El correo electrónico es inválido', 'yanbal'); ?>
                </small>
            </div>

            <div class="form-input-item col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                <select name="form-consideracion" id="inputConsideracion" class="form-control form-consideracion-input">
                    <option value="" selected disabled><?php _e('Como vendedor(a) te considerarías', 'yanbal'); ?></option>
                    <option value="Experimentado">Experimentado</option>
                    <option value="Entusiasta">Entusiasta</option>
                    <option value="Aprendíz">Aprendíz</option>
                    <option value="Sin Experiencia">Sin Experiencia</option>
                </select>
                <small id="errorConsideracion" class="invalid-feedback  error-consideracion d-none">
                    <?php _e('Debe seleccionar un valor', 'yanbal'); ?>
                </small>
            </div>
            <div class="form-input-item col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                <select name="form-capital" id="inputCapital" class="form-control form-capital-input">
                    <option value="" selected disabled><?php _e('¿Dispones de un capital mínimo de inversión de $50?', 'yanbal'); ?></option>
                    <option value="Si">Si</option>
                    <option value="No">No</option>
                    <option value="Requiero Préstamo">Requiero Préstamo</option>
                </select>
                <small id="errorCapital" class="invalid-feedback  error-capital d-none">
                    <?php _e('Debe seleccionar un valor', 'yanbal'); ?>
                </small>
            </div>
            <div class="form-input-item col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                <select name="form-interes" id="inputInteres" class="form-control form-interes-input">
                    <option value="" selected disabled><?php _e('¿Cuál es tu interés por convertirte en "Style Advisor"?', 'yanbal'); ?></option>
                    <option value="Complementar Ingresos">Complementar Ingresos</option>
                    <option value="Cambiar de profesión/oficio">Cambiar de profesión/oficio</option>
                    <option value="Pasatiempo">Pasatiempo</option>
                </select>
                <small id="errorInteres" class="invalid-feedback  error-interes d-none">
                    <?php _e('Debe seleccionar un valor', 'yanbal'); ?>
                </small>
            </div>
            <div class="form-input-item col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <label for="checkNews"><input id="checkNews" type="checkbox" value="subscribe"><?php _e('Quiero recibir las últimas noticias y promociones de Yanbal.', 'yanbal'); ?></label>
            </div>
            <div class="form-input-item form-input-submit col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <button id="formFullSubmit" type="submit" class="btn btn-hero" type="button"><?php _e("¡Quiero saber más!", "yanbal"); ?></button>
                <div class="loader-css d-none"></div>
            </div>
        </div>
    </div>
</form>