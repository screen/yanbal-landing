<?php
/**
* Template Name: Pagina Agradecimiento 
*
* @package yanbal
* @subpackage yanbal-mk01-theme
* @since Mk. 1.0
*/
?>
<?php get_header('empty'); ?>
<?php the_post(); ?>
<main class="container-fluid" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">
    <div class="row">
        <?php $img_bg = get_post_meta(get_the_ID(), 'ybl_welcome_bg', true); ?>
        <section id="post-<?php the_ID(); ?>" class="welcome-main-container page-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12" role="article" itemscope itemtype="http://schema.org/BlogPosting" style="background: url(<?php echo $img_bg; ?>)">
            <div class="container">
                <div class="row">
                    <div class="thanks-container section-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12" data-aos="fade" data-aos-delay="150">
                        <svg class="thanks-svg" version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 130.2 130.2">
                            <circle class="path circle" fill="none" stroke="#DC582A" stroke-width="7" stroke-miterlimit="10" cx="65.1" cy="65.1" r="62.1" />
                            <polyline class="path check" fill="none" stroke="#DC582A" stroke-width="7" stroke-linecap="round" stroke-miterlimit="10" points="100.2,40.2 51.5,88.8 29.8,67.5 " />
                        </svg>
                        <?php the_content(); ?>
                        <a href="<?php echo get_post_meta(get_the_ID(), 'ybl_button_url', true); ?>" title="<?php echo get_post_meta(get_the_ID(), 'ybl_button_text', true); ?>" class="btn btn-md btn-hero">
                            <?php echo get_post_meta(get_the_ID(), 'ybl_button_text', true); ?>
                        </a>
                    </div>
                </div>
            </div>
        </section>
    </div>
</main>
<?php get_footer('empty'); ?>