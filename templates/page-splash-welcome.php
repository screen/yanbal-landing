<?php
/**
* Template Name: Pagina Bienvenidos 
*
* @package yanbal
* @subpackage yanbal-mk01-theme
* @since Mk. 1.0
*/
?>
<?php get_header('empty'); ?>
<?php the_post(); ?>
<main class="container-fluid" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">
    <div class="row">
        <?php $img_bg = get_post_meta(get_the_ID(), 'ybl_welcome_bg', true); ?>
        <section id="post-<?php the_ID(); ?>" class="welcome-main-container page-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12" role="article" itemscope itemtype="http://schema.org/BlogPosting" style="background: url(<?php echo $img_bg; ?>)">
            <div class="container">
                <div class="row">
                    <div class="welcome-title section-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12" data-aos="fade" data-aos-delay="150">
                        <?php the_content(); ?>
                    </div>
                    <?php $arr_sites = get_post_meta(get_the_ID(), 'ybl_welcome_sites_group', true); ?>
                    <?php $i = 1; ?>
                    <?php foreach ($arr_sites as $site) { ?>
                    <?php $delay = 250 * $i; ?>
                    <article class="welcome-sections-container-item col-xl col-lg col-md col-sm-12 col-12" data-aos="fade-up" data-aos-delay="<?php echo $delay; ?>">
                        <div class="welcome-site-wrapper">
                            <?php $bg_banner_id = $site['bg_image_id']; ?>
                            <?php $bg_banner = wp_get_attachment_image_src($bg_banner_id, 'full', false); ?>
                            <img itemprop="logo" content="<?php echo $bg_banner[0]; ?>" src="<?php echo $bg_banner[0]; ?>" title="<?php echo get_post_meta($bg_banner_id, '_wp_attachment_image_alt', true); ?>" alt="<?php echo get_post_meta($bg_banner_id, '_wp_attachment_image_alt', true); ?>" class="img-fluid img-back" width="<?php echo $bg_banner[1]; ?>" height="<?php echo $bg_banner[2]; ?>" />
                            <a href="<?php echo $site['url']; ?>" class="welcome-sections-wrapper" title="<?php echo $site['title']; ?>">
                                <h2><?php echo $site['title']; ?></h2>
                            </a>
                        </div>
                    </article>
                    <?php $i++; } ?>
                </div>
            </div>
        </section>
    </div>
</main>
<?php get_footer('empty'); ?>