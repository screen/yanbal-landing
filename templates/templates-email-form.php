<div class="sendinblue-form-container">
    <form id="formSubscribe" class="sendinblue-form-container-wrapper">
        <input id="listID" type="hidden" name="listID" value="<?php echo get_post_meta(get_the_ID(), 'ybl_popup_sendinblue', true); ?>">
        <input type="email" class="form-control form-email-input" placeholder="<?php _e("Correo Electrónico.", "yanbal"); ?>" aria-label="<?php _e("Escribe tu Correo Electrónico.", "yanbal"); ?>" aria-describedby="button-addon2" />
        <button id="formSubscribeSubmit" type="submit" class="btn btn-outline-secondary" type="button" id="button-addon2"><?php _e("¡Comienza Ahora!", "yanbal"); ?></button>
        <div id="errorEmail" class="error-thanks-email invalid-feedback d-none">
            <?php _e('El correo no es válido, vuelva a intentarlo', 'yanbal'); ?>
        </div>
        <div class="loader-css d-none"></div>
    </form>
</div>