<?php
/**
* Template Name: Pagina Landing B2C 
*
* @package yanbal
* @subpackage yanbal-mk01-theme
* @since Mk. 1.0
*/
?>
<?php get_header(); ?>
<?php the_post(); ?>
<main class="container-fluid" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">
    <div class="row">
        <?php $img_bg = get_post_meta(get_the_ID(), 'ybl_main_bg', true); ?>
        <section id="post-<?php the_ID(); ?>" class="b2c-main-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12" role="article" itemscope itemtype="http://schema.org/BlogPosting" style="background: url(<?php echo $img_bg; ?>)">
            <div class="container">
                <div class="row align-items-center">
                    <picture class="b2c-image-container col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                        <?php the_post_thumbnail('full', array('class' => 'img-fluid'));  ?>
                    </picture>
                    <div class="b2c-content-section col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                        <?php the_content(); ?>
                    </div>
                </div>
            </div>
        </section>
        <div class="main-popup-container">
            <div class="main-popup-wrapper">
                <div class="container">
                    <div class="row">
                        <div class="row align-items-center">
                            <picture class="popup-image-container col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                            <?php $bg_banner_id = get_post_meta(get_the_ID(), 'ybl_popup_image_id', true); ?>
                            <?php $bg_banner = wp_get_attachment_image_src($bg_banner_id, 'full', false); ?>
                            <img itemprop="logo" content="<?php echo $bg_banner[0]; ?>" src="<?php echo $bg_banner[0]; ?>" title="<?php echo get_post_meta($bg_banner_id, '_wp_attachment_image_alt', true); ?>" alt="<?php echo get_post_meta($bg_banner_id, '_wp_attachment_image_alt', true); ?>" class="img-fluid img-back" width="<?php echo $bg_banner[1]; ?>" height="<?php echo $bg_banner[2]; ?>" />
                            </picture>
                            <div class="popup-content-section col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                                <?php echo apply_filters( 'the_content', get_post_meta(get_the_ID(), 'ybl_popup_content', true)); ?>
                                <?php echo get_template_part('templates/templates-email-form'); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</main>
<?php get_footer(); ?>