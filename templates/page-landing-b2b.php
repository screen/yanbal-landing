<?php
/**
* Template Name: Pagina Landing B2B 
*
* @package yanbal
* @subpackage yanbal-mk01-theme
* @since Mk. 1.0
*/
?>
<?php get_header('empty'); ?>
<?php the_post(); ?>
<main class="container-fluid" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">
    <div class="row">
        <?php $img_bg = get_post_meta(get_the_ID(), 'ybl_main_bg', true); ?>
        <section id="post-<?php the_ID(); ?>" class="b2b-main-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12" role="article" itemscope itemtype="http://schema.org/BlogPosting" style="background: url(<?php echo $img_bg; ?>)">
            <div class="container">
                <div class="row align-items-center">
                    <picture class="b2b-image-container col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                        <?php the_post_thumbnail('full', array('class' => 'img-fluid'));  ?>
                    </picture>
                    <div class="b2b-content-section col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                        <?php the_content(); ?>
                        <a href="<?php echo get_post_meta(get_the_ID(), 'ybl_button_url', true); ?>" title="<?php echo get_post_meta(get_the_ID(), 'ybl_button_text', true); ?>" class="btn btn-md btn-hero">
                            <?php echo get_post_meta(get_the_ID(), 'ybl_button_text', true); ?>
                        </a>
                    </div>
                </div>
            </div>
        </section>

        <section class="b2b-about-section col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="container">
                <div class="row">
                    <div class="b2b-title-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <?php $bg_banner_id = get_post_meta(get_the_ID(), 'ybl_about_icon_id', true); ?>
                        <?php $bg_banner = wp_get_attachment_image_src($bg_banner_id, 'full', false); ?>
                        <img itemprop="logo" content="<?php echo $bg_banner[0]; ?>" src="<?php echo $bg_banner[0]; ?>" title="<?php echo get_post_meta($bg_banner_id, '_wp_attachment_image_alt', true); ?>" alt="<?php echo get_post_meta($bg_banner_id, '_wp_attachment_image_alt', true); ?>" class="img-fluid img-back" width="<?php echo $bg_banner[1]; ?>" height="<?php echo $bg_banner[2]; ?>" />
                        <h2><?php echo get_post_meta( get_the_ID(), 'ybl_about_title', true); ?></h2>
                        <?php echo apply_filters('the_content', get_post_meta( get_the_ID(), 'ybl_about_intro', true));?>
                    </div>
                    <?php $group_about = get_post_meta( get_the_ID(), 'ybl_about_box_group', true); ?>
                    <?php $i = 1; ?>
                    <?php foreach ($group_about as $item) { ?>
                    <?php $class = ($i%2 == 0) ? 'even' : 'odd'; ?>
                    <div class="b2b-box-item <?php echo $class; ?> col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <?php $bg_banner_id = $item['image_id']; ?>
                        <?php $bg_banner = wp_get_attachment_image_src($bg_banner_id, 'full', false); ?>
                        <img itemprop="logo" content="<?php echo $bg_banner[0]; ?>" src="<?php echo $bg_banner[0]; ?>" title="<?php echo get_post_meta($bg_banner_id, '_wp_attachment_image_alt', true); ?>" alt="<?php echo get_post_meta($bg_banner_id, '_wp_attachment_image_alt', true); ?>" class="img-fluid img-back" width="<?php echo $bg_banner[1]; ?>" height="<?php echo $bg_banner[2]; ?>" />
                        <div class="b2b-box-item-wrapper">
                            <h2><?php echo $item['title']; ?></h2>
                            <?php echo apply_filters('the_content', $item['content']);?>
                        </div>
                    </div>
                    <?php $i++; } ?>
                </div>
            </div>
        </section>

        <section class="b2b-benefits-section col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="container">
                <div class="row">
                    <div class="b2b-title-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <?php $bg_banner_id = get_post_meta(get_the_ID(), 'ybl_benefits_icon_id', true); ?>
                        <?php $bg_banner = wp_get_attachment_image_src($bg_banner_id, 'full', false); ?>
                        <img itemprop="logo" content="<?php echo $bg_banner[0]; ?>" src="<?php echo $bg_banner[0]; ?>" title="<?php echo get_post_meta($bg_banner_id, '_wp_attachment_image_alt', true); ?>" alt="<?php echo get_post_meta($bg_banner_id, '_wp_attachment_image_alt', true); ?>" class="img-fluid img-back" width="<?php echo $bg_banner[1]; ?>" height="<?php echo $bg_banner[2]; ?>" />
                        <h2><?php echo get_post_meta( get_the_ID(), 'ybl_benefits_title', true); ?></h2>
                    </div>
                    <?php $group_benefits = get_post_meta( get_the_ID(), 'ybl_benefits_group', true); ?>
                    <?php $i = 1; ?>
                    <?php foreach ($group_benefits as $item) { ?>
                    <div class="benefits-item col-xl col-lg col-md-6 col-sm-6 col-12">
                        <div class="benefits-item-wrapper">
                            <?php $bg_banner_id = $item['image_id']; ?>
                            <?php $bg_banner = wp_get_attachment_image_src($bg_banner_id, 'full', false); ?>
                            <img itemprop="logo" content="<?php echo $bg_banner[0]; ?>" src="<?php echo $bg_banner[0]; ?>" title="<?php echo get_post_meta($bg_banner_id, '_wp_attachment_image_alt', true); ?>" alt="<?php echo get_post_meta($bg_banner_id, '_wp_attachment_image_alt', true); ?>" class="img-fluid img-back" width="<?php echo $bg_banner[1]; ?>" height="<?php echo $bg_banner[2]; ?>" />
                            <?php echo apply_filters('the_content', $item['content']);?>
                        </div>
                    </div>
                    <?php $i++; } ?>
                </div>
            </div>
        </section>

        <?php $img_bg = get_post_meta(get_the_ID(), 'ybl_inst_main_bg', true); ?>
        <section class="b2b-inst-section col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12" style="background: url(<?php echo $img_bg; ?>)">
            <div class="container">
                <div class="row justify-content-md-center">
                    <div class="b2b-title-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <?php $bg_banner_id = get_post_meta(get_the_ID(), 'ybl_inst_icon_id', true); ?>
                        <?php $bg_banner = wp_get_attachment_image_src($bg_banner_id, 'full', false); ?>
                        <img itemprop="logo" content="<?php echo $bg_banner[0]; ?>" src="<?php echo $bg_banner[0]; ?>" title="<?php echo get_post_meta($bg_banner_id, '_wp_attachment_image_alt', true); ?>" alt="<?php echo get_post_meta($bg_banner_id, '_wp_attachment_image_alt', true); ?>" class="img-fluid img-back" width="<?php echo $bg_banner[1]; ?>" height="<?php echo $bg_banner[2]; ?>" />
                        <h2><?php echo get_post_meta( get_the_ID(), 'ybl_inst_title', true); ?></h2>
                    </div>

                    <?php $group_inst = get_post_meta( get_the_ID(), 'ybl_inst_group', true); ?>
                    <?php $i = 1; ?>
                    <?php foreach ($group_inst as $item) { ?>
                    <div class="inst-item inst-item-<?php echo $i; ?> col-xl col-lg col-md-6 col-sm-12 col-12">
                        <div class="big-number">0<?php echo $i; ?></div>
                        <?php $bg_banner_id = $item['image_id']; ?>
                        <?php $bg_banner = wp_get_attachment_image_src($bg_banner_id, 'full', false); ?>
                        <img itemprop="logo" content="<?php echo $bg_banner[0]; ?>" src="<?php echo $bg_banner[0]; ?>" title="<?php echo get_post_meta($bg_banner_id, '_wp_attachment_image_alt', true); ?>" alt="<?php echo get_post_meta($bg_banner_id, '_wp_attachment_image_alt', true); ?>" class="img-fluid img-back" width="<?php echo $bg_banner[1]; ?>" height="<?php echo $bg_banner[2]; ?>" />
                        <div class="inst-item-wrapper">
                            <h2><?php echo $item['title']; ?></h2>
                            <?php echo apply_filters('the_content', $item['content']); ?>
                        </div>
                    </div>
                    <?php $i++; } ?>
                    <div class="b2b-button-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <a href="<?php echo get_post_meta(get_the_ID(), 'ybl_inst_button_url', true); ?>" title="<?php echo get_post_meta(get_the_ID(), 'ybl_inst_button_text', true); ?>" class="btn btn-md btn-hero">
                            <?php echo get_post_meta(get_the_ID(), 'ybl_inst_button_text', true); ?>
                        </a>
                    </div>
                </div>
            </div>
        </section>

        <section class="b2b-test-section col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="container">
                <div class="row">
                    <div class="b2b-title-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <?php $bg_banner_id = get_post_meta(get_the_ID(), 'ybl_test_icon_id', true); ?>
                        <?php $bg_banner = wp_get_attachment_image_src($bg_banner_id, 'full', false); ?>
                        <img itemprop="logo" content="<?php echo $bg_banner[0]; ?>" src="<?php echo $bg_banner[0]; ?>" title="<?php echo get_post_meta($bg_banner_id, '_wp_attachment_image_alt', true); ?>" alt="<?php echo get_post_meta($bg_banner_id, '_wp_attachment_image_alt', true); ?>" class="img-fluid img-back" width="<?php echo $bg_banner[1]; ?>" height="<?php echo $bg_banner[2]; ?>" />
                        <h2><?php echo get_post_meta( get_the_ID(), 'ybl_test_title', true); ?></h2>
                    </div>
                </div>
                <div class="row no-gutters">
                    <?php $group_test = get_post_meta( get_the_ID(), 'ybl_test_group', true); ?>
                    <?php $i = 1; ?>
                    <?php foreach ($group_test as $item) { ?>
                    <div class="test-item col-xl col-lg col-md col-sm-6 col-12">
                        <?php $bg_banner_id = $item['image_id']; ?>
                        <?php $bg_banner = wp_get_attachment_image_src($bg_banner_id, 'full', false); ?>
                        <img itemprop="logo" content="<?php echo $bg_banner[0]; ?>" src="<?php echo $bg_banner[0]; ?>" title="<?php echo get_post_meta($bg_banner_id, '_wp_attachment_image_alt', true); ?>" alt="<?php echo get_post_meta($bg_banner_id, '_wp_attachment_image_alt', true); ?>" class="img-fluid img-back" width="<?php echo $bg_banner[1]; ?>" height="<?php echo $bg_banner[2]; ?>" />
                        <div class="test-item-wrapper">
                            <?php echo apply_filters('the_content', $item['content']); ?>
                            <h3><?php echo $item['author']; ?></h3>
                        </div>
                    </div>
                    <?php $i++; } ?>
                </div>
            </div>
        </section>

        <section id="contact" class="b2b-contact-section col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="container">
                <div class="row">
                    <div class="b2b-title-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <?php $bg_banner_id = get_post_meta(get_the_ID(), 'ybl_contact_icon_id', true); ?>
                        <?php $bg_banner = wp_get_attachment_image_src($bg_banner_id, 'full', false); ?>
                        <img itemprop="logo" content="<?php echo $bg_banner[0]; ?>" src="<?php echo $bg_banner[0]; ?>" title="<?php echo get_post_meta($bg_banner_id, '_wp_attachment_image_alt', true); ?>" alt="<?php echo get_post_meta($bg_banner_id, '_wp_attachment_image_alt', true); ?>" class="img-fluid img-back" width="<?php echo $bg_banner[1]; ?>" height="<?php echo $bg_banner[2]; ?>" />
                        <h2><?php echo get_post_meta( get_the_ID(), 'ybl_contact_title', true); ?></h2>
                        <?php echo apply_filters('the_content', get_post_meta( get_the_ID(), 'ybl_contact_intro', true)); ?>
                    </div>
                    <?php echo get_template_part('templates/templates-email-fullform'); ?>
                </div>
            </div>
        </section>
    </div>
</main>
<?php get_footer('empty'); ?>