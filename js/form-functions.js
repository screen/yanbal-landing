var formThanks = document.getElementById('formSubscribe'),
    formThanksBtn = document.getElementById('formSubscribeSubmit'),
    formFull = document.getElementById('formFull'),
    formFullBtn = document.getElementById('formFullSubmit'),
    validForm = true;

function validateEmail(email) {
    var re = /\S+@\S+\.\S+/;
    return re.test(email);
}


function telephoneCheck(str) {
    var patt = new RegExp(/^(\+{0,})(\d{0,})([(]{1}\d{1,3}[)]{0,}){0,}(\s?\d+|\+\d{2,3}\s{1}\d+|\d+){1}[\s|-]?\d+([\s|-]?\d+){1,2}(\s){0,}$/gm);
    return patt.test(str);
}

/* CUSTOM ON LOAD FUNCTIONS */
function formDocumentCustomLoad() {
    "use strict";
    console.log('Form Functions Correctly Loaded');

    if (formThanks != null) {
        formThanksBtn.addEventListener('click', function(e) {
            e.preventDefault();
            validForm = true;
            var elements = document.getElementsByClassName('form-email-input');
            var errorText = document.getElementById('errorEmail');
            if (elements[0].value == '') {
                errorText.classList.remove('d-none');
                validForm = false;
            } else {
                if (validateEmail(elements[0].value) == false) {
                    errorText.classList.remove('d-none');
                    validForm = false;
                } else {
                    errorText.classList.add('d-none');
                    validForm = true;
                }
            }

            if (validForm == true) {
                submitForm(validForm);
            }
        });
    }

    if (formFull != null) {
        formFullBtn.addEventListener('click', function(e) {
            e.preventDefault();
            validForm = true;
            var elements = document.getElementById('inputName');
            var errorText = document.getElementsByClassName('error-name');
            if (elements.value == '') {
                errorText[0].classList.remove('d-none');
                errorText[0].innerHTML = custom_admin_url.error_nombre;
                validForm = false;
            } else {
                if (elements.value == '') {
                    errorText[0].classList.remove('d-none');
                    errorText[0].innerHTML = custom_admin_url.invalid_nombre;
                    validForm = false;
                } else {
                    errorText[0].classList.add('d-none');
                    errorText[0].innerHTML = '';
                    validForm = true;
                }
            }

            var elements = document.getElementById('inputLast');
            var errorText = document.getElementsByClassName('error-last');
            if (elements.value == '') {
                errorText[0].classList.remove('d-none');
                errorText[0].innerHTML = custom_admin_url.error_nombre;
                validForm = false;
            } else {
                if (elements.value == '') {
                    errorText[0].classList.remove('d-none');
                    errorText[0].innerHTML = custom_admin_url.invalid_nombre;
                    validForm = false;
                } else {
                    errorText[0].classList.add('d-none');
                    errorText[0].innerHTML = '';
                    validForm = true;
                }
            }

            var elements = document.getElementById('inputEmail');
            var errorText = document.getElementsByClassName('error-email');
            if (elements.value == '') {
                errorText[0].classList.remove('d-none');
                errorText[0].innerHTML = custom_admin_url.error_email;
                validForm = false;
            } else {
                if (validateEmail(elements.value) == false) {
                    errorText[0].classList.remove('d-none');
                    errorText[0].innerHTML = custom_admin_url.invalid_email;
                    validForm = false;
                } else {
                    errorText[0].classList.add('d-none');
                    errorText[0].innerHTML = '';
                    validForm = true;
                }
            }

            if (validForm == true) {
                submitFullForm();
            }
        });
    }
}

document.addEventListener("DOMContentLoaded", formDocumentCustomLoad, false);

function submitForm() {
    var emailForm = document.getElementsByClassName('form-email-input');
    var listID = document.getElementById('listID');
    var info = 'action=subscribe_contact&email_contact=' + emailForm[0].value + '&list_id=' + listID.value;
    var elements = document.getElementsByClassName('loader-css');
    elements[0].classList.toggle("d-none");
    /* SEND AJAX */
    newRequest = new XMLHttpRequest();
    newRequest.open('POST', custom_admin_url.ajax_url, true);
    newRequest.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
    newRequest.onload = function() {
        console.log(custom_admin_url.b2c_thanks_url);
        window.location = custom_admin_url.b2c_thanks_url;
    };
    newRequest.send(info);
}


function submitFullForm() {
    var nameForm = document.getElementById('inputName');
    var lastForm = document.getElementById('inputLast');
    var emailForm = document.getElementById('inputEmail');
    var inputConsideracion = document.getElementById('inputConsideracion');
    var inputCapital = document.getElementById('inputCapital');
    var inputInteres = document.getElementById('inputInteres');

    var listForm = document.getElementById('listID');
    var info = 'action=subscribe_contact&email_contact=' + emailForm.value + '&email_name=' + nameForm.value + '&email_last=' + lastForm.value + '&email_consideracion=' + inputConsideracion.value + '&email_capital=' + inputCapital.value + '&email_interes=' + inputInteres.value + '&list_id=' + listForm.value;
    var elements = document.getElementsByClassName('loader-css');
    elements[0].classList.toggle("d-none");

    newRequest = new XMLHttpRequest();
    newRequest.open('POST', custom_admin_url.ajax_url, true);
    newRequest.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
    newRequest.onload = function() {
        console.log(custom_admin_url.b2b_thanks_url);
        window.location = custom_admin_url.b2b_thanks_url;
    };
    newRequest.send(info);
}