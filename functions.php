<?php

/* --------------------------------------------------------------
    ENQUEUE AND REGISTER CSS
-------------------------------------------------------------- */

require_once('includes/wp_enqueue_styles.php');

/* --------------------------------------------------------------
    ENQUEUE AND REGISTER JS
-------------------------------------------------------------- */

if (!is_admin()) add_action('wp_enqueue_scripts', 'yanbal_jquery_enqueue');
function yanbal_jquery_enqueue()
{
    wp_deregister_script('jquery');
    wp_deregister_script('jquery-migrate');
    if ($_SERVER['REMOTE_ADDR'] == '::1') {
        /*- JQUERY ON LOCAL  -*/
        wp_register_script('jquery', get_template_directory_uri() . '/js/jquery.min.js', false, '3.5.1', false);
        /*- JQUERY MIGRATE ON LOCAL  -*/
        wp_register_script('jquery-migrate', get_template_directory_uri() . '/js/jquery-migrate.min.js',  array('jquery'), '3.1.0', false);
    } else {
        /*- JQUERY ON WEB  -*/
        wp_register_script('jquery', 'https://code.jquery.com/jquery-3.5.1.min.js', false, '3.5.1', false);
        /*- JQUERY MIGRATE ON WEB  -*/
        wp_register_script('jquery-migrate', 'https://code.jquery.com/jquery-migrate-3.3.1.js', array('jquery'), '3.3.1', true);
    }
    wp_enqueue_script('jquery');
    wp_enqueue_script('jquery-migrate');
}

/* NOW ALL THE JS FILES */

require_once('includes/wp_enqueue_scripts.php');

/* --------------------------------------------------------------
    ADD CUSTOM WALKER BOOTSTRAP
-------------------------------------------------------------- */

add_action('after_setup_theme', 'yanbal_register_navwalker');
function yanbal_register_navwalker()
{
    require_once('includes/class-wp-bootstrap-navwalker.php');
}

/* --------------------------------------------------------------
    ADD CUSTOM WORDPRESS FUNCTIONS
-------------------------------------------------------------- */

require_once('includes/wp_custom_functions.php');

/* --------------------------------------------------------------
    ADD REQUIRED WORDPRESS PLUGINS
-------------------------------------------------------------- */

require_once('includes/class-tgm-plugin-activation.php');
require_once('includes/class-required-plugins.php');

/* --------------------------------------------------------------
    ADD CUSTOM WOOCOMMERCE OVERRIDES
-------------------------------------------------------------- */
if (class_exists('WooCommerce')) {
    require_once('includes/wp_woocommerce_functions.php');
}

/* --------------------------------------------------------------
    ADD JETPACK COMPATIBILITY
-------------------------------------------------------------- */
if (defined('JETPACK__VERSION')) {
    require_once('includes/wp_jetpack_functions.php');
}

/* --------------------------------------------------------------
    ADD NAV MENUS LOCATIONS
-------------------------------------------------------------- */

register_nav_menus(array(
    'header_menu' => __('Menu Header - Principal', 'yanbal'),
    'footer_menu' => __('Menu Footer - Principal', 'yanbal')
));

/* --------------------------------------------------------------
    ADD DYNAMIC SIDEBAR SUPPORT
-------------------------------------------------------------- */

add_action('widgets_init', 'yanbal_widgets_init');

function yanbal_widgets_init()
{
    register_sidebar(array(
        'name' => __('Sidebar Principal', 'yanbal'),
        'id' => 'main_sidebar',
        'description' => __('Estos widgets seran vistos en las entradas y páginas del sitio', 'yanbal'),
        'before_widget' => '<li id="%1$s" class="widget %2$s">',
        'after_widget'  => '</li>',
        'before_title'  => '<h2 class="widgettitle">',
        'after_title'   => '</h2>',
    ));

    register_sidebars(4, array(
        'name'          => __('Pie de Página %d', 'yanbal'),
        'id'            => 'sidebar_footer',
        'description'   => __('Estos widgets seran vistos en el pie de página del sitio', 'yanbal'),
        'before_widget' => '<li id="%1$s" class="widget %2$s">',
        'after_widget'  => '</li>',
        'before_title'  => '<h2 class="widgettitle">',
        'after_title'   => '</h2>'
    ));

    //    register_sidebar( array(
    //        'name' => __( 'Sidebar de la Tienda', 'yanbal' ),
    //        'id' => 'shop_sidebar',
    //        'description' => __( 'Estos widgets seran vistos en Tienda y Categorias de Producto', 'yanbal' ),
    //        'before_widget' => '<li id='%1$s' class='widget %2$s'>',
    //        'after_widget'  => '</li>',
    //        'before_title'  => '<h2 class='widgettitle'>',
    //        'after_title'   => '</h2>',
    //    ) );
}



/* --------------------------------------------------------------
    ADD CUSTOM METABOX
-------------------------------------------------------------- */

require_once('includes/wp_custom_metabox.php');

/* --------------------------------------------------------------
    ADD CUSTOM POST TYPE
-------------------------------------------------------------- */

require_once('includes/wp_custom_post_type.php');

/* --------------------------------------------------------------
    ADD CUSTOM THEME CONTROLS
-------------------------------------------------------------- */

require_once('includes/wp_custom_theme_control.php');

/* --------------------------------------------------------------
    ADD CUSTOM IMAGE SIZE
-------------------------------------------------------------- */
if (function_exists('add_theme_support')) {
    add_theme_support('post-thumbnails');
    set_post_thumbnail_size(9999, 400, true);
}
if (function_exists('add_image_size')) {
    add_image_size('avatar', 100, 100, true);
    add_image_size('blog_img', 276, 217, true);
    add_image_size('single_img', 636, 297, true);
}

/* --------------------------------------------------------------
    WIDGET COUNTRY
-------------------------------------------------------------- */
class country_selector_widget extends WP_Widget
{

    function __construct()
    {
        parent::__construct(

            'country_selector_widget',
            __('Selector de País', 'yanbal'),
            array('description' => __('Activa un selector de pais en un combobox', 'yanbal'),)
        );
    }

    public function widget($args, $instance)
    {
        $title = apply_filters('widget_title', $instance['title']);

        echo $args['before_widget'];
        if (!empty($title))
            echo $args['before_title'] . $title . $args['after_title'];
        ob_start();
?>
        <select name="country_selector" id="country_selector" class="form-control custom-country-selector">
            <option value="" selected disabled><?php _e('Seleccionar País', 'yanbal'); ?></option>
            <option value="#">Bolivia</option>
            <option value="#">Colombia</option>
            <option value="#">Ecuador</option>
            <option value="#">España</option>
            <option value="#">Guatemala</option>
            <option value="#">Italia</option>
            <option value="#">México</option>
            <option value="#">Perú</option>
            <option value="#">USA</option>
        </select>
    <?php
        $content = ob_get_clean();
        echo $content;

        echo $args['after_widget'];
    }

    public function form($instance)
    {
        if (isset($instance['title'])) {
            $title = $instance['title'];
        } else {
            $title = __('New title', 'yanbal');
        }
    ?>
        <p>
            <label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr($title); ?>" />
        </p>
    <?php
    }

    public function update($new_instance, $old_instance)
    {
        $instance = array();
        $instance['title'] = (!empty($new_instance['title'])) ? strip_tags($new_instance['title']) : '';
        return $instance;
    }
}

/* --------------------------------------------------------------
    WIDGET COUNTRY
-------------------------------------------------------------- */
class social_networks_widget extends WP_Widget
{

    function __construct()
    {
        parent::__construct(

            'social_networks_widget',
            __('Redes Sociales', 'yanbal'),
            array('description' => __('Coloca las redes sociales registradas en "Personalizar"', 'yanbal'),)
        );
    }

    public function widget($args, $instance)
    {
        $title = apply_filters('widget_title', $instance['title']);

        echo $args['before_widget'];
        if (!empty($title))
            echo $args['before_title'] . $title . $args['after_title'];
        ob_start();
    ?>
        <ul>
            <?php $social_settings = get_option('ybl_social_settings'); ?>
            <li>
                <a href="<?php echo $social_settings['facebook']; ?>" title="<?php _e('Visítanos en Facebook', 'yanbal'); ?>"><i class="fa fa-facebook"></i></a>
            </li>
            <li>
                <a href="<?php echo $social_settings['instagram']; ?>" title="<?php _e('Visítanos en Instagram', 'yanbal'); ?>"><i class="fa fa-instagram"></i></a>
            </li>
            <li>
                <a href="<?php echo $social_settings['youtube']; ?>" title="<?php _e('Visítanos en YouTube', 'yanbal'); ?>"><i class="fa fa-youtube-play"></i></a>
            </li>
            <li>
                <a href="<?php echo $social_settings['linkedin']; ?>" title="<?php _e('Visítanos en LinkedIn', 'yanbal'); ?>"><i class="fa fa-linkedin"></i></a>
            </li>
        </ul>
    <?php
        $content = ob_get_clean();
        echo $content;

        echo $args['after_widget'];
    }

    public function form($instance)
    {
        if (isset($instance['title'])) {
            $title = $instance['title'];
        } else {
            $title = __('New title', 'yanbal');
        }
    ?>
        <p>
            <label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr($title); ?>" />
        </p>
<?php
    }

    public function update($new_instance, $old_instance)
    {
        $instance = array();
        $instance['title'] = (!empty($new_instance['title'])) ? strip_tags($new_instance['title']) : '';
        return $instance;
    }
}

/* --------------------------------------------------------------
    WIDGET REGISTRY
-------------------------------------------------------------- */
function wpb_load_widget()
{
    register_widget('country_selector_widget');
    register_widget('social_networks_widget');
}
add_action('widgets_init', 'wpb_load_widget');

/* --------------------------------------------------------------
    SENDINBLUE INTEGRATION
-------------------------------------------------------------- */
function sendinblue_add_contact($email, $nombre, $apellido, $list_id, $consideracion, $interes, $capital)
{
    $sendinblue_settings = get_option('ybl_sendinblue_settings');
    $createContact['email'] = $email;
    $createContact['attributes'] = array('FIRSTNAME' => $nombre, 'LASTNAME' => $apellido, 'CONSIDERACION' => $consideracion, 'CAPITAL' => $capital, 'INTERES' => $interes);
    $createContact['listIds'] = array(intval($list_id));
    $createContact['emailBlacklisted'] = false;
    $createContact['smsBlacklisted'] = false;
    $createContact['updateEnabled'] = false;

    $cliente = curl_init();
    curl_setopt($cliente, CURLOPT_URL, "https://api.sendinblue.com/v3/contacts");
    curl_setopt($cliente, CURLOPT_HTTPHEADER, array('Accept: application/json', 'Content-Type: application/json', 'api-key:' . $sendinblue_settings['apikey']));
    curl_setopt($cliente, CURLOPT_POST, 1);
    curl_setopt($cliente, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($cliente, CURLOPT_POSTFIELDS, json_encode($createContact));
    $respuesta = json_decode(curl_exec($cliente));
    $http_code = curl_getinfo($cliente, CURLINFO_HTTP_CODE);
    curl_close($cliente);
    if ($http_code == 400) {
        $createContact['email'] = $email;
        $createContact['attributes'] = array('FIRSTNAME' => $nombre, 'LASTNAME' => $apellido, 'CONSIDERACION' => $consideracion, 'CAPITAL' => $capital, 'INTERES' => $interes);
        $createContact['listIds'] = array(intval($list_id));
        $createContact['emailBlacklisted'] = false;
        $createContact['smsBlacklisted'] = false;
        $createContact['updateEnabled'] = true;
        curl_setopt($cliente, CURLOPT_POSTFIELDS, json_encode($createContact));
        $cliente = curl_init();
        curl_setopt($cliente, CURLOPT_URL, "https://api.sendinblue.com/v3/contacts");
        curl_setopt($cliente, CURLOPT_HTTPHEADER, array('Accept: application/json', 'Content-Type: application/json', 'api-key:' . $sendinblue_settings['apikey']));
        curl_setopt($cliente, CURLOPT_POST, 1);
        curl_setopt($cliente, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($cliente, CURLOPT_POSTFIELDS, json_encode($createContact));
        $respuesta = json_decode(curl_exec($cliente));
        $http_code = curl_getinfo($cliente, CURLINFO_HTTP_CODE);
        curl_close($cliente);
    }
    
    $content = true;
    return $content;
    wp_die();
}

add_action('wp_ajax_subscribe_contact', 'subscribe_contact_handler');
add_action('wp_ajax_nopriv_subscribe_contact', 'subscribe_contact_handler');

function subscribe_contact_handler()
{
    $email = $_POST['email_contact'];
    $list_id = $_POST['list_id'];

    if (isset($_POST['email_name'])) {
        $nombre = $_POST['email_name'];
    } else {
        $nombre = '';
    }

    if (isset($_POST['email_last'])) {
        $apellido = $_POST['email_last'];
    } else {
        $apellido = '';
    }

    if (isset($_POST['email_consideracion'])) {
        $consideracion = $_POST['email_consideracion'];
    } else {
        $consideracion = '';
    }

    if (isset($_POST['email_capital'])) {
        $capital = $_POST['email_capital'];
    } else {
        $capital = '';
    }

    if (isset($_POST['email_interes'])) {
        $interes = $_POST['email_interes'];
    } else {
        $interes = '';
    }

    $content = sendinblue_add_contact($email, $nombre, $apellido, $list_id, $consideracion, $interes, $capital);

    ob_start();
    $logo = get_template_directory_uri() . '/images/logo-orange.png';
    require_once get_theme_file_path('/templates/contact-email.php');
    $body = ob_get_clean();
    $body = str_replace([
        '{name}',
        '{lastname}',
        '{email}',
        '{consideracion}',
        '{interes}',
        '{capital}',
        '{logo}'
    ], [
        $nombre,
        $apellido,
        $email,
        $consideracion,
        $capital,
        $interes,
        $logo
    ], $body);

    $path = ABSPATH . WPINC . '/class-phpmailer.php';

    if (file_exists($path)) {
        require_once($path);
        $mail = new PHPMailer;
    } else {
        require_once ABSPATH . WPINC . '/PHPMailer/PHPMailer.php';
        $mail = new PHPMailer\PHPMailer\PHPMailer;
    }

    $mail->isHTML(true);
    $mail->Body = $body;
    $mail->CharSet = 'UTF-8';
    $mail->addAddress('yanbalusalatino@gmail.com');
    $mail->addAddress('rochoa@screen.com.ve');
    $mail->addAddress('nefrain@gmail.com');
    $mail->setFrom("noreply@{$_SERVER['SERVER_NAME']}", esc_html(get_bloginfo('name')));
    $mail->Subject = esc_html__('Yanbal: Nuevo Suscriptor', 'tisserie');

    if (!$mail->send()) {
        wp_send_json_success(esc_html__("Thank You for your interest in our products, you will be contacted shortly.", 'tisserie'), 200);
    } else {
        wp_send_json_success(esc_html__("Thank You for your interest in our products, you will be contacted shortly.", 'tisserie'), 200);
    }
}
